<?php
/**
 * This part of template displaying event start date on top left corner of event detail page.
 *
 * Override this template by copying it to yourtheme/simple_event_planner/single-event/event-start-date.php
 * 
 * @version     1.0.0
 * @since       1.3.0 
 * @author      PressTigers
 * @package     Simple_Event_Planner
 * @subpackage  Simple_Event_Planner/templates/single-event
 */
global $post; ?>

<div class="single-date">
    <?php
    $start_date = sep_get_the_event_start_date();
    $split_date = explode(',', $start_date);

    if (!empty($start_date)) {
        $date_array = array();
        $date_array = explode(' ', $split_date[0]);
        ?>
        <strong>
            <?php
            echo $date_array[0] . '</br>';
            ?>
        </strong>
        <?php
        echo $date_array[1] . ',' . $split_date[1];
    }
    ?>
</div>

<?php
$single_start_date = ob_get_clean();
/**
 * Modify Event Start Date - Event Date Template. 
 *                                       
 * @since   1.3.0
 * 
 * @param   html    $single_start_date   Event Start Date HTML.                   
 */
echo apply_filters('sep_event_start_date_template', $single_start_date);