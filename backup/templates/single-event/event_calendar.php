<?php
/**
 * Template displaying calendar links on event detail page.
 *
 * Override this template by copying it to yourtheme/simple_event_planner/single-event/event_calendar.php
 * 
 * @version     1.0.0
 * @since       1.3.0 
 * @author      PressTigers
 * @package     Simple_Event_Planner
 * @subpackage  Simple_Event_Planner/templates/single-event
 */

global $post;

$start_date = date('Ymd', strtotime(sep_get_the_event_start_date()));
$end_date = date('Ymd', strtotime(sep_get_the_event_end_date()));
date_default_timezone_set('UTC');
$start_time = date('\THis\Z', strtotime(str_replace('-', '/', sep_get_the_event_start_time())));
$end_time = date('\THis\Z', strtotime(str_replace('-', '/', sep_get_the_event_end_time())));
?>
<a href="https://calendar.google.com/calendar/render?action=TEMPLATE&text=<?php echo get_the_title(); ?>&dates=<?php echo $start_date . $start_time; ?>/<?php echo $end_date . $end_time; ?>&details=For+details,+link+here:+<?php echo the_permalink(); ?>&location=<?php echo sep_get_the_event_venue(); ?>&sf=true&output=xml" target="_blank" rel="nofollow"> Google Calendar </a> </br>
<a href="<?php echo SIMPLE_EVENT_PLANNER_PLUGIN_URL . '/templates/single-event/ical.php' ?>?startDate=<?php echo $start_date; ?>&amp;endDate=<?php echo $end_date; ?>&amp;startTime=<?php echo $start_time; ?>&amp;endTime=<?php echo $end_time; ?>&amp;uid=<?php echo $post->ID; ?>&amp;location=<?php echo sep_get_the_event_venue(); ?>&amp;subject=<?php echo get_the_title(); ?>&amp;url= <?php the_permalink() ?>">iCal Calendar</a>