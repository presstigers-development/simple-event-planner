<?php
/**
 * The template for displaying event start date on listing page.
 *
 * Override this template by copying it to yourtheme/simple_event_planner/event-listing/event-start-date.php
 * 
 * @version     1.0.0
 * @since       1.3.0 
 * @author      PressTigers
 * @package     Simple_Event_Planner
 * @subpackage  Simple_Event_Planner/templates/event-listing
 */
global $post; 
if(''!== sep_get_the_event_start_date()){
?>

<!-- start Date image -->
<div class="date">
    <!-- start date fixing -->
    <div class="date-style">
        <?php
        $start_date = sep_get_the_event_start_date();
        $split_date = explode(',', $start_date);
        $date_array = array();
        if (!empty($start_date)) {
            $date_array = explode(' ', $split_date[0]);
            echo '<strong>' . $date_array[0] . '<br />' . '</strong>' . $date_array[1]. ',' . $split_date[1];
        }
        ?>
    </div><!-- start date fixing -->
</div><!-- end Date image -->
<?php
}
$event_list_start_date = ob_get_clean();
/**
 * Modify Event Listing Start Date - Event Start Date Template. 
 *                                       
 * @since   1.3.0
 * 
 * @param   html  $event_list_start_date   Date HTML.                   
 */
echo apply_filters('sep_event_list_start_date_template', $event_list_start_date);