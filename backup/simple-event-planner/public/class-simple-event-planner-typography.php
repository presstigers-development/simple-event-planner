<?php
if (!defined('ABSPATH')) {
    exit;
} // Exit if accessed directly
/**
 * Simple_Event_Planner_Typography Class
 * 
 * This is used to implement typography settings. This class applying custom 
 * typography on event listing and calendar page. 
 *
 * @link        https://wordpress.org/plugins/simple-event-planner/
 * @since       1.1.0
 *
 * @package     Simple_Event_Planner
 * @subpackage  Simple_Event_Planner/public
 * @author      PressTigers <support@presstigers.com>
 */

class Simple_Event_Planner_Typography {

    /**
     * Initialize the class and set its properties.
     *
     * @since   1.1.0
     */
    public function __construct() {

        // Hook -> Trigger user defined styles in head section
        add_action('wp_head', array($this, 'event_planner_typography'));
    }

    /**
     * This function implementing style changes on calender and event listing
     * 
     * @since  1.1.0
     * 
     * @global array $sep_event_options
     * 
     * @return void
     */
    public function event_planner_typography() {
        $sep_event_options = get_option('sep_event_options');

        /* General Styling */
        $event_primary_color = isset($sep_event_options['event_primary_color']) ? $sep_event_options['event_primary_color'] : '#3399fe';

        $event_secondary_color = isset($sep_event_options['event_secondary_color']) ? $sep_event_options['event_secondary_color'] : '#f7f7f7';

        $event_title_color = isset($sep_event_options['event_title_color']) ? $sep_event_options['event_title_color'] : '#363e40';

        $event_content_color = isset($sep_event_options['event_content_color']) ? $sep_event_options['event_content_color'] : '#ababab';
        ?>
        <style type="text/css">

            /* Primary Classes */
            .sep-page .list-view article .date{
                background-color: <?php echo $event_primary_color; ?>;
            }
            .sep-page .grid-view article .date{
                background-color: <?php echo $event_primary_color; ?>;
            }
            .sep-page .sep-detail .event-schedule .countdown .main-digit-wrapp .cs-digit{
                color: <?php echo $event_primary_color; ?>;
            }
            .sep-page a, .sep-event-organizer span a:focus{
                color: <?php echo $event_primary_color; ?>;
            }
            
            /* Remaining */
            .sep-wrap .eventCalendar-wrap .eventsCalendar-slider .eventsCalendar-monthWrap .eventsCalendar-daysList .eventsCalendar-day-header{
                background:  <?php echo $event_primary_color; ?>;
            }
            .sep-wrap .eventCalendar-wrap .eventsCalendar-slider .eventsCalendar-monthWrap .eventsCalendar-daysList li.dayWithEvents a{
                background:  <?php echo $event_primary_color; ?>;
            }
            .sep-wrap .eventCalendar-wrap .eventsCalendar-slider .eventsCalendar-monthWrap .showAsWeek .eventsCalendar-day.today.dayWithEvents{
                background-color: <?php echo $event_primary_color; ?>;
            }
            .sep-search .sep-button, .sep-search .sep-button-cal{
                background-color: <?php echo $event_primary_color; ?>;
            }
            .sep-wrap .eventCalendar-wrap .eventsCalendar-slider .eventsCalendar-monthWrap .eventsCalendar-daysList li.today{
                background-color: <?php echo $event_primary_color; ?>;
            }
            .sep-wrap .eventCalendar-wrap .eventsCalendar-slider .eventsCalendar-monthWrap .eventsCalendar-daysList li.current {
                background-color: <?php echo $event_primary_color; ?>;
            }
            .sep-wrap .eventCalendar-wrap .eventsCalendar-list-wrap .eventsCalendar-list .eventsCalendar-noEvents p {
                color: <?php echo $event_primary_color; ?>;
            }
            .sep-wrap .sep-event-listing .event-list .date-style{
                background-color:<?php echo $event_primary_color; ?>;
                opacity: 0.5;
            }
            .sep-wrap  .sep-event-info i{
                color: <?php echo $event_primary_color; ?>;
            }
            .sep-wrap .sep-segments-style .item:before{
                color: <?php echo $event_primary_color; ?>;
            }
            .sep-wrap .sep-segments-style .timeline{
                background: <?php echo $event_primary_color; ?>;
            }

            /* Secondary */
            .sep-page .grid-view article{
                background-color: <?php echo $event_secondary_color ?>;
            }
            .sep-page .list-view article{
                background-color: <?php echo $event_secondary_color ?>;
            }

            /* Heading and Sub Heading*/
            .sep-page .list-view article .description h4 a {
                color: <?php echo $event_title_color ?>;
            }
            .sep-page .grid-view article .description h4 a {
                color: <?php echo $event_title_color ?>;
            }
            .sep-page .sep-detail .event-organizer h3, .sep-page .sep-detail .single-event-time h3, .sep-page .sep-detail .event-venue h3, .sep-page .sep-detail .single-segments h3  {
               color: <?php echo $event_title_color ?>;  
            }
            .sep-page .sep-detail .event-organizer table tr td:first-child{
                color: <?php echo $event_title_color ?>;   
            }
            .sep-page .sep-detail .single-event-time .event-date-time strong{
               color: <?php echo $event_title_color ?>;  
            }
            .sep-page .sep-detail .event-description .event-title h2 {
             color: <?php echo $event_title_color ?>;     
            }
            
            /* Remaining */
            .sep-wrap .eventCalendar-wrap .eventsCalendar-list-wrap .eventsCalendar-list li .eventTitle {
                color: <?php echo $event_title_color ?>;
            }
            .sep-single-event h2, .sep-single-event h3{
                color: <?php echo $event_title_color ?>;
            }
            .eventsCalendar-currentTitle .monthTitle{
                color: <?php echo $event_title_color ?>;
            }
            .sep-wrap .eventCalendar-wrap .eventsCalendar-list-wrap .eventsCalendar-subtitle {
                color: <?php echo $event_title_color ?>;
            }
            .sep-wrap .sep-event-listing .sep-post-title a{
                color: <?php echo $event_title_color ?>;
            }

            /* Content */
            .sep-page .sep-detail .event-description .single-event-description p {
               color: <?php echo $event_content_color ?>; 
            }
            .sep-page .sep-detail .event-organizer table tr td:last-child{
                color: <?php echo $event_content_color ?>;
            }
            .sep-page .sep-detail .single-event-time .event-date-time time{
                color: <?php echo $event_content_color ?>;
            }
            .sep-page .sep-detail .single-segments .segments-style .item{
                color: <?php echo $event_content_color ?>;
            }
            .sep-page .main-digit-wrapp .countdown-period{
                color: <?php echo $event_content_color ?>;
            }
            .sep-page .sep-detail .event-venue{
                 color: <?php echo $event_content_color ?>;
            }
            .sep-page .list-view article .description .location a {
               color: <?php echo $event_content_color ?>;  
            }
            .sep-page .list-view article .description .time time {
               color: <?php echo $event_content_color ?>;  
            }
            
            
            .sep-event-description .single-event-description p{
                color: <?php echo $event_content_color ?>;
            }
            .sep-event-schedule time{
                color: <?php echo $event_content_color ?>;
            }
            .sep-wrap .sep-single-event-time time{
                color: <?php echo $event_content_color ?>;
            }
            .sep-event-organizer span, .sep-event-venue span{
                color: <?php echo $event_content_color ?>;
            }
            .sep-wrap .eventCalendar-wrap .eventsCalendar-list-wrap .eventsCalendar-list li time em{
                color: <?php echo $event_content_color ?>;
            }
            .sep-wrap .eventCalendar-wrap .eventsCalendar-list-wrap .eventsCalendar-list li time small{
                color: <?php echo $event_content_color ?>;
            }
            .sep-wrap .eventCalendar-wrap .eventsCalendar-list-wrap .eventsCalendar-list li .eventDesc {
                color: <?php echo $event_content_color ?>;
            }
            .sep-wrap .event-plan .event-schedule a{
                color: <?php echo $event_content_color ?>;
            }
            .sep-wrap .event-plan .event-schedule time{
                color: <?php echo $event_content_color ?>;
            }
        </style> 
        <?php
    }

}

new Simple_Event_Planner_Typography();