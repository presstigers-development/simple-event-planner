<?php
/**
 * The template for displaying events in list view/layout.
 *
 * Override this template by copying it to yourtheme/simple_event_planner/content-event-listing-list-view.php
 * 
 * @version     1.0.0
 * @since       1.1.0 
 * @author      PressTigers
 * @package     Simple_Event_Planner
 * @subpackage  Simple_Event_Planner/templates
 */
global $post;
?>

<!-- Start Event Listing
================================================== -->
<article>
    <div class="row"> 
        <?php
        /**
         * Template -> Featured Image:
         * 
         * - Event Featured Image
         */
        get_simple_event_planner_template('event-listing/featured-image.php');
        ?>
        <div class="col-md-12">
            <?php
            /**
             * Template -> Event Start Date:
             * 
             * - Event Start Title
             */
            get_simple_event_planner_template('event-listing/event-start-date.php');
            ?>
            <div class="description">
                <?php
                /**
                 * Template -> Title:
                 * 
                 * - Event Title
                 */
                get_simple_event_planner_template('event-listing/title.php');

                /**
                 * Template -> Venue:
                 * 
                 * - Event Venue
                 */
                get_simple_event_planner_template('event-listing/venue.php');

                /**
                 * Template -> Date:
                 * 
                 * - Event Date
                 */
                get_simple_event_planner_template('event-listing/date.php');
                ?>    
            </div>
        </div>
    </div>
</article>  
<!-- ==================================================
End Event Listing -->