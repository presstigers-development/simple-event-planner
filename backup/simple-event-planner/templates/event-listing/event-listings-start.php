<?php
/**
 * Event list start
 *
 * Override this template by copying it to yourtheme/simple_event_planner/event-listing/event-listing-start.php
 * 
 * @version     1.1.0
 * @since       1.1.0
 * @author      PressTigers
 * @package     Simple_Event_Planner
 * @subpackage  Simple_Event_Planner/templates/event-listing
 */

$sep_event_options = get_option('sep_event_options');
$list_layout = $sep_event_options['sep_event_layout'];

// Displays User Defined Layout
if ('grid-view' === $list_layout) {
    echo '<div class="grid-view">';
} else {
    echo '<div class="list-view">';
}


