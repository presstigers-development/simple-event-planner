<?php
/**
 * Displayed when no events are found, matching the current query.
 *
 * Override this template by copying it to yourtheme/simple_event_planner/event-listing/content-no-events-found.php
 *
 * @version     1.0.0
 * @since       1.1.0
 * @author      PressTigers
 * @package     Simple_Event_Planner
 * @subpackage  Simple_Event_Planner/templates/event-listing
 */

if ( ! defined( 'ABSPATH' ) ) { exit; } // Exit if accessed directly

// Get Current Page Slug           
$slugs = sep_get_slugs();
$page_slug = $slugs[0];
$slug = ( get_option('permalink_structure') ) ? $page_slug : '';
echo '<div class="no_found"><p>' .__( 'No events found.', 'simple-event-planner' ).'</p><p><a href="' . esc_url(home_url('/')) . $slug . '" class="btn btn-primary"> Go Back to Event Listing</a></p></div>';