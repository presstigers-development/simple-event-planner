<?php
/**
 * This template contains event organiser's contact number.
 *
 * Override this template by copying it to yourtheme/simple_event_planner/single-event/event-detail/conatct.php
 * 
 * @version     1.0.0
 * @since       1.1.0
 * @author      PressTigers
 * @package     Simple_Event_Planner
 * @subpackage  Simple_Event_Planner/templates/single-event/event-detail
 */
global $post;

if ('' <> sep_get_the_organizer_contact()) {
    ?>

    <!-- Start Event Organizer contact
    ================================================== -->
    <tr>
        <td scope="row">
            <?php _e('Phone', 'simple-event-planner'); ?>
        </td>
        <td>
            <?php echo sep_get_the_organizer_contact(); ?> 
        </td>
    </tr>
    <!-- ==================================================
    End Event Organizer contact -->

    <?php
}

$contact = ob_get_clean();

/**
 * Modify Event Contact - Contact Template. 
 *                                       
 * @since   1.3.0
 * 
 * @param   html    $contact   Event Contact HTML.                   
 */
echo apply_filters('sep_contact_template', $contact);