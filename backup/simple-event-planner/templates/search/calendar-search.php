<?php
/**
 * The template containig event location search for calendar event listing page.
 *
 * Override this template by copying it to yourtheme/simple_event_planner/search/calendar-search.php
 * 
 * @version     1.0.0
 * @since       1.1.0 
 * @author      PressTigers
 * @package     Simple_Event_Planner
 * @subpackage  Simple_Event_Planner/templates/search
 */
global $post;
?>

<!-- Start Calendar Search
================================================== -->
<div class="search">
    <div class="form-group">
        <!-- Search Form -->
        <form class="form-group" method="post" action="javascript:search_events('<?php echo admin_url("admin-ajax.php") ?>','<?php echo $rand_id; ?>')" id="search-form<?php echo $rand_id; ?>">
            <input class="form-control" type="text" placeholder="Search Location" id="loc-addres<?php echo $rand_id; ?>" name="loc-addres<?php echo $rand_id; ?>" value=""/>
            <button class="input-group-addon submit-location" type="submit" id="<?php echo $rand_id; ?>">
                <i class="fa fa-search" aria-hidden="true"></i>
            </button>
            <input type="hidden" id="location-address<?php echo $rand_id; ?>" name="location-address<?php echo $rand_id; ?>" value="" />
            <input type="hidden" id="loc-search<?php echo $rand_id; ?>" name="loc-search<?php echo $rand_id; ?>" value="false" />
            <input type="hidden" id="event-cat<?php echo $rand_id; ?>" name="event-cat<?php echo $rand_id; ?>" value="<?php echo trim($event_category); ?>" />
        </form>
        <!-- Search Form -->
    </div>
</div>
<div class="alert alert-success" role="alert"> <strong>Well done!</strong> You successfully read this important alert message. </div>
<div class="alert alert-info" role="alert"> <strong>Heads up!</strong> This alert needs your attention, but it's not super important. </div> 
<div class="alert alert-warning" role="alert"> <strong>Warning!</strong> Better check yourself, you're not looking too good. </div> 
<div class="alert alert-danger" role="alert"> <strong>Oh snap!</strong> Change a few things up and try submitting again. </div> 

<!-- ==================================================
End Calendar Search -->

<?php
$calendar_search = ob_get_clean();

/**
 * Modify Calendar Seacrch - Calendar Search Template. 
 *                                       
 * @since   1.3.0
 * 
 * @param   html    $calendar_search   Calendar Search HTML.                   
 */
echo apply_filters('sep_calendar_search_template', $calendar_search);
