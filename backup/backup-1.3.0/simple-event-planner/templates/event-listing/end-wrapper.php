<?php 
/**
 * Event list end
 *
 * Override this template by copying it to yourtheme/simple_event_planner/event-listing/end-wrapper.php
 * 
 * @version     2.0.0
 * @since       1.1.0 
 * @since       1.3.0 
 * @author      PressTigers
 * @package     Simple_Event_Planner
 * @subpackage  Simple_Event_Planner/templates/event-listing
 */
?>
<div class="clear"> </div> </div>
<?php
$end_wrapper = ob_get_clean();

/**
 * Modify End Wrapper Template. 
 *                                       
 * @since   1.3.0
 * 
 * @param   html    $end_wrapper   End wrapper HTML.                   
 */
echo apply_filters('sep_no_events_found_template', $end_wrapper);