<?php
/**
 * This template contains event organizer's email.
 *
 * Override this template by copying it to yourtheme/simple_event_planner/single-event/event-detail/email.php
 * 
 * @version     2.0.0
 * @since       1.1.0 
 * @since       1.3.0 
 * @author      PressTigers
 * @package     Simple_Event_Planner
 * @subpackage  Simple_Event_Planner/templates/single-event/event-detail
 */
global $post;

if ('' <> sep_get_the_organizer_email()) {
    ?> 

    <!-- Start Event Organizer's Email 
    ================================================== -->
    <tr>
        <td scope="row">
            <?php _e('Email', 'simple-event-planner'); ?>
        </td>
        <td>
            <a href="mailto:<?php echo sep_get_the_organizer_email(); ?> "> <?php echo sep_get_the_organizer_email(); ?> </a>
        </td>
    </tr>
    <!-- ==================================================
    End Event Organizer's Email -->

    <?php
}

$email = ob_get_clean();

/**
 * Modify Event Email - Email Template. 
 *                                       
 * @since   1.3.0
 *
 * @param   html  $email   Event Email HTML.                   
 */
echo apply_filters('sep_email_template', $email);