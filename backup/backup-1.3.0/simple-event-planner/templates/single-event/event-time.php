<?php
/**
 * This part of template displaying event start and end time on event detail page.
 *
 * Override this template by copying it to yourtheme/simple_event_planner/single-event/event-time.php
 * 
 * @version     2.0.0
 * @since       1.1.0 
 * @since       1.3.0 
 * @author      PressTigers
 * @package     Simple_Event_Planner
 * @subpackage  Simple_Event_Planner/templates/single-event
 */
global $post;

// Start Event's Start and End Date
if ( '' !== sep_get_the_event_start_date() && '' !== sep_get_the_event_end_date() ) { ?>
    <div class="event-date-time">
    <strong> <?php _e('Date:', 'simple-event-planner'); ?> </strong>
    <time> <?php echo sep_get_the_event_start_date() . ' - to - ' . sep_get_the_event_end_date() . sep_get_the_event_two_dates_diff(); ?>
    </time>
    </div>
<?php } elseif ('' !== sep_get_the_event_start_date()) {
    ?>
    <div class="event-date-time">
    <strong><?php _e('Date:', 'simple-event-planner'); ?> </strong>
    <time> <?php echo sep_get_the_event_start_date() ?> </time>
    </div>
    <?php
}

// Start Event's Start and End Time
if ('' !== sep_get_the_event_start_time() && '' !== sep_get_the_event_end_time()) {
    ?>
    <div class="event-date-time">
        <strong><?php _e('Time:', 'simple-event-planner'); ?></strong>
        <time><?php echo sep_get_the_event_start_time() . ' - to - ' . sep_get_the_event_end_time() . sep_get_the_event_time_Zone() ?></time>
    </div>
<?php } elseif ('' !== sep_get_the_event_start_time()) {
    ?>
    <div class="event-date-time">
        <strong><?php _e('Time:', 'simple-event-planner'); ?></strong>
        <time><?php echo sep_get_the_event_start_time() . sep_get_the_event_time_Zone(); ?></time>
    </div>
    <?php
}

$event_time = ob_get_clean();

/**
 * Modify Event Time  - Event Time Template. 
 *                                       
 * @since   1.3.0
 * 
 * @param   html    $event_time   Event Time HTML.                   
 */
echo apply_filters( 'sep_event_time_template', $event_time );