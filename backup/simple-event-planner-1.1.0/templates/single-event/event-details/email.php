<?php
/**
 * This template contains event organizer's email.
 *
 * Override this template by copying it to yourtheme/simple_event_planner/single-event/event-detail/email.php
 * 
 * @version     1.0.0
 * @since       1.1.0
 * @author      PressTigers
 * @package     Simple_Event_Planner
 * @subpackage  Simple_Event_Planner/templates/single-event/event-detail
 */
global $post;

if ('' <> sep_get_the_organizer_email()) { ?> 

    <!-- Start Event organizer's mail 
    ================================================== -->
    <div class="sep-col-md-4 sep-col-sm-4 sep-col-xs-12">
        <h4> <?php _e('Email', 'simple-event-planner'); ?> </h4>
        <span><a href="mailto:<?php echo sep_get_the_organizer_email(); ?> "> <?php echo sep_get_the_organizer_email(); ?> </a></span>
    </div>
    <!-- ==================================================
    End Event organizer's mail -->
    
    <?php
}