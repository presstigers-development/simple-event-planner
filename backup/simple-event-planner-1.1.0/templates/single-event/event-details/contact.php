<?php
/**
 * This template contains event organizer conatct number.
 *
 * Override this template by copying it to yourtheme/simple_event_planner/single-event/event-detail/conatct.php
 * 
 * @version     1.0.0
 * @since       1.1.0
 * @author      PressTigers
 * @package     Simple_Event_Planner
 * @subpackage  Simple_Event_Planner/templates/single-event/event-detail
 */
global $post;

if ('' <> sep_get_the_organizer_contact()) { ?>
    
    <!-- Start Event Organizer contact
    ================================================== -->
    <div class="sep-col-md-4 sep-col-sm-4 sep-col-xs-12">
        <h4> <?php _e('Phone', 'simple-event-planner'); ?> </h4>
        <span> <?php echo sep_get_the_organizer_contact(); ?> </span>
    </div>
    <!-- ==================================================
    End Event Organizer contact -->
    
<?php }