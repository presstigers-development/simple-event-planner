<?php
/**
 * This template contains segment's setion.
 *
 * Override this template by copying it to yourtheme/simple_event_planner/single-event/event-detail/segments.php
 * 
 * @version     1.0.0
 * @since       1.1.0
 * @author      PressTigers
 * @package     Simple_Event_Planner
 * @subpackage  Simple_Event_Planner/templates/single-event/event-detail
 */
global $post;

$segments = sep_get_event_segment();
if (is_array($segments) && '' <> $segments[0]) { ?>

    <!-- Start Event Segments 
    ================================================== -->
    <div class="sep-event-segment">
        <h3> <?php _e('Segments:', 'simple-event-planner'); ?> </h3>
        <ul>
            <?php
            if (!empty($segments)) {
                $i = 1;
                foreach ($segments as $key => $value) {
                    echo '<li>' . " $value " . "\n" . '</li>';
                    $i++;
                }
            }
            ?>
        </ul>
    </div>
    <!-- ==================================================
    End Event Segments -->

<?php }