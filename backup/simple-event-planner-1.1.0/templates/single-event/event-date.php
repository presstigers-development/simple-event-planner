<?php
/**
 * This part of template displaying event start and end date on event detail page.
 *
 * Override this template by copying it to yourtheme/simple_event_planner/single-event/event-date.php
 * 
 * @version     1.0.0
 * @since       1.1.0
 * @author      PressTigers
 * @package     Simple_Event_Planner
 * @subpackage  Simple_Event_Planner/templates/single-event
 */
global $post;

// Start Event's Start and End Date
if ('' <> sep_get_the_event_start_date() && '' <> sep_get_the_event_end_date()) {
    ?>
    <div class="sep-col-md-6 sep-col-sm-6 sep-col-xs-12">
        <h4> <?php _e('Date:', 'simple-event-planner'); ?> </h4>
        <time> <?php echo sep_get_the_event_start_date() . ' - to - ' . sep_get_the_event_end_date() . sep_get_the_event_two_dates_diff(); ?></time>
    </div>
<?php } elseif ('' <> sep_get_the_event_start_date()) {
    ?>
    <div class="sep-col-md-6 sep-col-sm-6 sep-col-xs-12">
        <h4><?php _e('Date:', 'simple-event-planner'); ?> </h4>
        <time> <?php echo sep_get_the_event_start_date() ?> </time>
    </div>
<?php }