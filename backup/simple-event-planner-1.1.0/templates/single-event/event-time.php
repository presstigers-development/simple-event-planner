<?php
/**
 * This part of template displaying event start and end time on event detail page.
 *
 * Override this template by copying it to yourtheme/simple_event_planner/single-event/event-time.php
 * 
 * @version     1.0.0
 * @since       1.1.0
 * @author      PressTigers
 * @package     Simple_Event_Planner
 * @subpackage  Simple_Event_Planner/templates/single-event
 */
global $post;

// Start Event's Start and End Time
if ( '' != sep_get_the_event_start_time() && '' != sep_get_the_event_end_time() ) {
    ?>
    <div class="sep-col-md-6 sep-col-sm-6 sep-col-xs-12">
        <h4> <?php _e('Time:', 'simple-event-planner'); ?> </h4>
        <time> <?php echo sep_get_the_event_start_time() . ' - to - ' . sep_get_the_event_end_time() . sep_get_the_event_time_Zone() ?></time>
    </div>
<?php } elseif ( '' != sep_get_the_event_start_time() ) {
    ?>
    <div class="sep-col-md-6 sep-col-sm-6 sep-col-xs-12">
        <h4> <?php _e('Time:', 'simple-event-planner'); ?> </h4>
        <time> <?php echo sep_get_the_event_start_time() . sep_get_the_event_time_Zone(); ?> </time>
    </div>
<?php }