<?php
/**
 * This part of template displaying counter on event detail page.
 *
 * Override this template by copying it to yourtheme/simple_event_planner/single-event/event-counter.php
 * 
 * @version     1.0.0
 * @since       1.1.0
 * @author      PressTigers
 * @package     Simple_Event_Planner
 * @subpackage  Simple_Event_Planner/templates/single-event
 */

// Event Counter
if ('' != sep_get_the_event_start_date() || '' != sep_get_the_event_end_date() || '' != sep_get_the_event_start_time() || '' <> sep_get_the_event_end_time()) {
?>
<div class="sep-countdown sep-row">
    <div id="countdownwrapp" class="sep-col-md-5 sep-col-sm-7">
        <div id="countdown-underconstruction"></div>
        <div class="clearfix"></div>
    </div>        
</div>
<?php }