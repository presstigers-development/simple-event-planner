<?php
/**
 * This template contain catgory, organiser's detail and  segments also. Which are getting display on event detail page.
 *
 * Override this template by copying it to yourtheme/simple_event_planner/single-event/event-details.php
 * 
 * @version     1.0.0
 * @since       1.1.0
 * @author      PressTigers
 * @package     Simple_Event_Planner
 * @subpackage  Simple_Event_Planner/templates/single-event
 */
global $post;

/**
 * Template -> evet-category:
 * 
 * - Event Category
 */
get_simple_event_planner_template('single-event/event-details/category.php');

if ('' <> sep_get_the_event_organizer() || '' <> sep_get_the_organizer_contact() || '' <> sep_get_the_organizer_email()) { ?>

    <!-- Start Event Organizer Details 
    ================================================== -->
    <div class="sep-event-organizer">   
        <h3><?php _e('Organizer:', 'simple-event-planner'); ?></h3>
        <div class="sep-row">
            <?php
            /**
             * Template -> Name:
             * 
             * - Event organizer name
             */
            get_simple_event_planner_template('single-event/event-details/name.php');

            /**
             * Template -> Contact:
             * 
             * - Event organizer contact
             */
            get_simple_event_planner_template('single-event/event-details/contact.php');

            /**
             * Template -> Email:
             * 
             * - Event organizer email
             */
            get_simple_event_planner_template('single-event/event-details/email.php');
            ?>
        </div>    
    </div>
    <!-- ==================================================
    End Event Organizer Detail -->

<?php
}

/**
 * Template -> Segments:
 * 
 * - Event segment
 */
get_simple_event_planner_template('single-event/event-details/segments.php');