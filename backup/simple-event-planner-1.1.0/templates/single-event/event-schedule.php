<?php
/**
 * This part of template displaying counter and sehdual section of the event detail page.
 *
 * Override this template by copying it to yourtheme/simple_event_planner/single-event/event-schedule.php
 * 
 * @version     1.0.0
 * @since       1.1.0
 * @author      PressTigers
 * @package     Simple_Event_Planner
 * @subpackage  Simple_Event_Planner/templates/single-event
 */
global $post;
?>

<!-- Start Event Schedule 
================================================== -->
<div class="sep-event-schedule">
    <?php
    /**
     * Template -> event-counter:
     * 
     * - Event Counter
     */
    get_simple_event_planner_template('single-event/event-counter.php');

    if ('' != sep_get_the_event_start_date() || '' != sep_get_the_event_end_date() || '' != sep_get_the_event_start_time() || '' <> sep_get_the_event_end_time()) {
        ?>
        <h3> <?php _e('Schedule', 'simple-event-planner'); ?> </h3>
        <div class="sep-row">
            <?php
            /**
             * Template -> event-date:
             * 
             * - Event start and end date
             */
            get_simple_event_planner_template('single-event/event-date.php');

            /**
             * Template -> event-time:
             * 
             * - Event start and end time
             */
            get_simple_event_planner_template('single-event/event-time.php');
            ?>
        </div>
    <?php } ?>    
</div>
<!-- ==================================================
End Event Schedule section -->