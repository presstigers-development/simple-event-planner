<?php
/**
 * Template displayng featured image and descirption of event detail page.
 *
 * Override this template by copying it to yourtheme/simple_event_planner/single-event/event-description.php
 * 
 * @version     1.0.0
 * @since       1.1.0
 * @author      PressTigers
 * @package     Simple_Event_Planner
 * @subpackage  Simple_Event_Planner/templates/single-event
 */
global $post;
?>

<!-- Start Event Description 
================================================== -->
<div class="sep-event-description sep-row">

    <?php
    /**
     * Template -> Feature Image:
     * 
     * - Event Feature Image
     */
    get_simple_event_planner_template('single-event/featured-image.php'); ?>

    <!-- Event Description -->
    <?php $class = (sep_get_the_event_image() !== '') ? 'sep-col-md-9' : 'sep-col-md-12'; ?>
    
    <div class="single-event-description <?php echo $class; ?>">
        <?php the_content(); ?> 
    </div>    
</div>
<!-- ==================================================
End Event Description -->