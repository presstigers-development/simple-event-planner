<?php
/**
 * Calling event description template.
 * 
 * Override this template by copying it to yourtheme/simple_event_planner/content-single-event-listing.php
 * 
 * @version     1.0.0
 * @since       1.1.0
 * @author      PressTigers
 * @package     Simple_Event_Planner
 * @subpackage  Simple_Event_Planner/templates
 */
?>

<!-- Start Event listing
  ================================================== -->
<div class="single-event-listing">    
    <?php
    /**
     * single_event_listing_start hook
     *
     * @hooked event_counter_script_localization- 20
     * @hooked event_schedule - 30
     * 
     * @since   1.1.0
     */
    do_action('single_event_listing_start');

    /**
     * Template -> Event Description:
     * 
     * - Event Featured image
     * - Event Description
     */
    get_simple_event_planner_template('single-event/event-description.php');

    /**
     * single_event_listing_end hook
     * @hooked event_details - 20
     * @hooked event_venue - 30
     * 
     * @since   1.1.0
     */
    do_action('single_event_listing_end');
    ?>
</div>
<!-- ==================================================
 End Event listing -->