<?php
/**
 * The template for displaying event details on signle event deatil page.
 *
 * Override this template by copying it to yourtheme/simple_event_planner/single-event-listing.php
 *
 * @version     1.0.0
 * @since       1.1.0
 * @author      PressTigers
 * @package     Simple_Event_Planner
 * @subpackage  Simple_Event_Planner/templates
 */
get_header();
global $post;

/**
 * Hook -> sep_before_main_content
 * 
 * @hooked sep_event_listing_wrapper_start - 10 
 * - Output Opening div of Main Container.
 * - Output Opening div of Content Area.
 * 
 * @since  1.1.0
 */
do_action('sep_before_main_content');
?>

<!-- Start Content Wrapper
================================================== -->
<div class="sep-wrap">

    <!-- Start Event Details
    ================================================== -->
    <div class="sep-event-detail sep-single-event">

        <!-- Start Event Title
        ================================================== -->
        <div class="sep-event-title">
            <h2><?php echo apply_filters('sep_single_event_detail_page_title', get_the_title()); ?></h2>
        </div>
        <!-- ==================================================
        End Event Title -->

        <?php
        while (have_posts()) : the_post();
            /**
             * Template -> Content Single Event Listing:
             * 
             * - Event Counter
             * - Event Schedule
             * - Event Description 
             * - Event Organizer Details
             * - Event Venue & Map
             */
            get_simple_event_planner_template_part('content', 'single-event-listing');
        endwhile;
        ?>
    </div>
    <!-- ==================================================
    End Event Details -->

</div>
<!-- ==================================================
End Content Wrapper -->

<?php
/**
 * Hook -> sep_after_main_content
 * 
 * @hooked sep_event_listing_wrapper_end - 10 
 * - Output Closing div of Main Container.
 * - Output Closing div of Content Area.
 * 
 * @since  1.1.0
 */
do_action('sep_after_main_content');

get_footer();