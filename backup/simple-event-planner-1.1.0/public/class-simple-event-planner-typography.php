<?php if (!defined('ABSPATH')) { exit; } // Exit if accessed directly
/**
 * Simple_Event_Planner_Typography Class
 * 
 * This is used to implement typography settings. This class applying custom 
 * typography on event listing and calendar page. 
 *
 * @link        https://wordpress.org/plugins/simple-event-planner/
 * @since       1.1.0
 *
 * @package     Simple_Event_Planner
 * @subpackage  Simple_Event_Planner/public
 * @author      PressTigers <support@presstigers.com>
 */

class Simple_Event_Planner_Typography {

    /**
     * Initialize the class and set its properties.
     *
     * @since   1.1.0
     */
    public function __construct() {

        // Hook -> Trigger user defined styles in head section
        add_action( 'wp_head', array( $this, 'event_planner_typography' ) );
    }

    /**
     * This function implementing style changes on calender and event listing
     * 
     * @since  1.1.0
     * 
     * @global array $sep_event_options
     * 
     * @return void
     */
    public function event_planner_typography() {
        $sep_event_options = get_option('sep_event_options');

        /* General Styling */
        $event_primary_color = isset($sep_event_options['event_primary_color']) ? $sep_event_options['event_primary_color'] : '#3399fe';
        
        $event_secondary_color = isset($sep_event_options['event_secondary_color']) ? $sep_event_options['event_secondary_color'] : '#f7f7f7';
        
        $event_title_color = isset($sep_event_options['event_title_color']) ? $sep_event_options['event_title_color'] : '#363e40';
        
        $event_content_color = isset($sep_event_options['event_content_color']) ? $sep_event_options['event_content_color'] : '#ababab';
        ?>
        <style type="text/css">

            /* Primary Classes */
            .sep-wrap .sep-event-listing .map-style{
                background-color: <?php echo $event_primary_color; ?> !important;
            }
            .sep-wrap .eventCalendar-wrap .eventsCalendar-slider .eventsCalendar-monthWrap .eventsCalendar-daysList .eventsCalendar-day-header{
                background:  <?php echo $event_primary_color; ?> !important;
            }
            .sep-wrap .eventCalendar-wrap .eventsCalendar-slider .eventsCalendar-monthWrap .eventsCalendar-daysList li.dayWithEvents a{
                background:  <?php echo $event_primary_color; ?> !important;
            }
            .sep-single-event .main-digit-wrapp .cs-digit{
                color: <?php echo $event_primary_color; ?> !important;
            }
            .sep-event-organizer span a, .sep-event-organizer span a:focus{
                color: <?php echo $event_primary_color; ?> !important;
            }
            .sep-wrap .eventCalendar-wrap .eventsCalendar-slider .eventsCalendar-monthWrap .showAsWeek .eventsCalendar-day.today.dayWithEvents{
                background-color: <?php echo $event_primary_color; ?> !important;
            }

            /* Secondary */
            .sep-wrap .sep-event-listing article {
                background-color: <?php echo  $event_secondary_color ?> !important ;
            }

            /* Text Color */
            .sep-single-event h2, .sep-single-event h3{
                color: <?php echo  $event_title_color ?> !important;
            }
            .eventsCalendar-currentTitle .monthTitle{
                color: <?php echo  $event_title_color ?> !important;
            }

            /* Content */
            .sep-single-event .main-digit-wrapp .countdown-period{
                color: <?php echo  $event_content_color ?> !important;
            }
            .sep-event-description .single-event-description p{
                color: <?php echo  $event_content_color ?> !important;
            }
            .sep-event-schedule time{
                color: <?php echo  $event_content_color ?> !important;
            }
            .sep-event-organizer span, .sep-event-venue span{
                color: <?php echo  $event_content_color ?> !important ;
            }
             
        </style> 
        <?php
    }

}

new Simple_Event_Planner_Typography();