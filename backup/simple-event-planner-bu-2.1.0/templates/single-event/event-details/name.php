<?php
/**
 * This template contains event orgnaizer's name.
 *
 * Override this template by copying it to yourtheme/simple_event_planner/single-event/event-detail/name.php
 * 
 * @version     1.0.0
 * @since       1.1.0
 * @author      PressTigers
 * @package     Simple_Event_Planner
 * @subpackage  Simple_Event_Planner/templates/single-event/event-detail
 */
global $post;

if ('' <> sep_get_the_event_organizer()) { ?> 

    <!-- Start Event Organizer's Name
    ================================================== -->

    <tr>
        <th scope="row">
            <h4> <?php _e('Name', 'simple-event-planner'); ?> </h4>
        </th>
        <td>
            <span> <?php echo sep_get_the_event_organizer(); ?> </span>
        </td>
    </tr>
    <!-- ==================================================
    End Event Organizer's Name -->

<?php } 

$html_name = ob_get_clean();

/**
 * Modify Event's Organiser Name - Name Template. 
 *                                       
 * @since   2.1.0
 * 
 * @param   html    $html_name   Name HTML.                   
 */
echo apply_filters( 'sep_name_template', $html_name);