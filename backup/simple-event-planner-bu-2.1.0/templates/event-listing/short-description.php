<?php
/**
 * The template for displaying event description.
 *
 * Override this template by copying it to yourtheme/simple_event_planner/event-listing/short-description.php
 * 
 * @version     1.0.0
 * @since       1.1.0 
 * @author      PressTigers
 * @package     Simple_Event_Planner
 * @subpackage  Simple_Event_Planner/templates/event-listing
 */

global $post;
$col_class = ( '' == sep_get_the_event_image() ) ? 'sep-col-md-12' : 'sep-col-md-9'; ?>

<!-- Start Event Short Description
================================================== -->
<div class="event-list-description <?php echo $col_class; ?>">
    <p> <?php echo sep_get_the_excerpt(400, FALSE, ''); ?> </p>
</div>
<!-- ==================================================
End Event Short Description -->

<?php

$list_event_excerpt = ob_get_clean();

/**
 * Modify Event Excerpt - Short Description Template. 
 *                                       
 * @since   2.1.0
 * 
 * @param   html    $list_event_excerpt   Event Excerpt HTML.                   
 */
echo apply_filters( 'sep_short_description_template', $list_event_excerpt );