<?php
/**
 * The template for displaying event title.
 *
 * Override this template by copying it to yourtheme/simple_event_planner/event-listing/title.php
 * 
 * @version     1.0.0
 * @since       1.1.0 
 * @author      PressTigers
 * @package     Simple_Event_Planner
 * @subpackage  Simple_Event_Planner/templates/event-listing
 */
global $post;
?>

<!-- Start Event Title
================================================== -->
<h2 class="sep-post-title">
    <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
</h2>
<!-- ==================================================
End Event Title -->

<?php

$list_event_title = ob_get_clean();

/**
 * Modify Event Title - Schedule Template. 
 *                                       
 * @since   2.1.0
 * 
 * @param   html    $list_event_title   Title HTML.                   
 */
echo apply_filters( 'sep_meta_title_template', $list_event_title );