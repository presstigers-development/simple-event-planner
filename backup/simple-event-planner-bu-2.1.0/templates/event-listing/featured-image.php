<?php
/**
 * The template for displaying event featured image.
 *
 * Override this template by copying it to yourtheme/simple_event_planner/event-listing/featured-image.php
 * 
 * @version     1.0.0
 * @since       1.1.0 
 * @author      PressTigers
 * @package     Simple_Event_Planner
 * @subpackage  Simple_Event_Planner/templates/event-listing
 */
global $post;

if (sep_get_the_event_image()) { ?>

    <!-- Start Event Featured Image
    ================================================== -->
    <a target="blank" href="<?php the_permalink(); ?>">
        
        <!-- Featured Image -->
        <div class="event-list-image">
            <?php echo sep_get_the_event_image(); ?>
        </div>
        <div class="date-text">
        <?php
        /**
         * Template -> Date:
         * 
         * - Event Start
         */
        $start_date = sep_get_the_event_start_date();
        $split_date = explode(',', $start_date);
        $date_array = array();
        if( !empty( $start_date ) ) {
            $date_array = explode(' ', $split_date[0]);
            echo '<strong>'. $date_array[1] .'<br />' . '</strong>' . $date_array[0].$split_date[1];
        }
        ?><!-- End Event Featured Image -->
        </div>
        <div class="date-style"></div>
     </a>
    <!-- ==================================================
    End Event Featured Image -->
<?php
}

$list_feature_image = ob_get_clean();

/**
 * Modify Featured Image - Featured Image Template. 
 *                                       
 * @since   2.1.0
 * 
 * @param   html    $list_feature_image  Feature Image HTML.                   
 */
echo apply_filters( 'sep_list_featured_image_template', $list_feature_image );