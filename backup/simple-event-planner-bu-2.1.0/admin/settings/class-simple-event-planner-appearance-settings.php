<?php if (!defined('ABSPATH')) { exit; } // Exit if accessed directly
/**
 * Simple_Event_Planner_Appearance_Settings Class
 *
 * This is used to define event listing appearnce settings.
 * 
 * @link        https://wordpress.org/plugins/simple-event-planner/
 * @since       1.1.0
 * 
 * @package     Simple_Event_Planner
 * @subpackage  Simple_Event_Planner/admin/settings
 * @author      PressTigers <support@presstigers.com>
 */

class Simple_Event_Planner_Appearance_Settings{

    /**
     * Initialize the class and set its properties.
     *
     * @since   1.1.0
     */
    public function __construct() {
        
        // Filter -> Add Appearnce Settings Tab
        add_filter('sep_settings_tab_menus', array($this, 'add_settings_tab'), 40);
        
        // Hook - Add Appearnce Settings Section
        add_action('sep_appearance_settings', array($this, 'add_appearance_settings_section'));
    }
    
    /**
     * Add Gmap Api Key Settings Tab.
     *
     * @since    1.1.0
     * 
     * @param    array  $tabs  Settings Tab
     * @return   array  $tabs  Merge array of Settings Tab with Api Key Tab.
     */
    public function add_settings_tab($tabs) {
        
        $tabs['appearance'] = __( 'Appearance', 'simple-event-planner' );
        return $tabs;
    }
    
    /**
     * GMap Api Key Settings Section.
     *
     * @since    1.1.0
     *
     * @global   Object $post               Post Object
     * @global   array  $sep_event_options  Event Options Data for Settings.
     */
    public function add_appearance_settings_section() {
        global $post, $sep_event_options;
      
        $list_layout    = $sep_event_options['sep_event_layout'];
        $list_image     = $sep_event_options['sep_event_content'];
        $time_settings  = $sep_event_options['sep_time_format'];
        
         $grid_view = $list_view= $show_image = $hide_image = $hours_12 = $hours_24 ='';
         
        // Check Event Listing Layout
        if ('list-view' === $list_layout) {
                $list_view = 'checked';
            } elseif ('grid-view' === $list_layout) {
                $grid_view = 'checked';
            } else {
                
                // Default List View
                $list_view = 'checked';
            }
            
        // Check Event Listing Image    
        if ('show-image' === $list_image) {
                $show_image = 'checked';
            } elseif ('hide-image' === $list_image) {
                $hide_image = 'checked';
            } else {
                
                // Default Show Image
                $show_image = 'checked';
            }
            
         // Check Time Format Settings
         if ('24-hours' === $time_settings) {
                $hours_24 = 'checked';
            } elseif ('12-hours' === $time_settings) {
                $hours_12 = 'checked';
            } else {
                
                // Default 24 Hours
                $hours_24 = 'checked';
            }
        ?>

        <!-- Appearance Settings Header -->
        <div class="theme-header">
            <h1> <?php _e('Appearance Settings', 'simple-event-planner'); ?> </h1>
        </div>
      
        
        <ul class="form-elements">
            <li class="field-label">
                <label> <?php _e('Event Listing Configuration', 'simple-event-planner') ?>  </label>
            </li>
        </ul>
        
        <ul class="form-elements">
            <li class="field-label">
                <label> <?php _e('Event Listing Layout', 'simple-event-planner') ?>  </label>
            </li>
            <li class="element-field">
                <input type="radio" name="sep_event_layout" value="list-view"<?php echo $list_view; ?> > <?php _e('List View', 'simple-event-planner') ?> 
            </li>
            <li class="element-field">
            <input type="radio" name="sep_event_layout" value="grid-view"<?php echo $grid_view; ?> >  <?php _e('Grid View', 'simple-event-planner') ?> 
            </li>
        </ul>
        
        <ul class="form-elements">
            <li class="field-label">
                <label> <?php _e('Event Listing Content', 'simple-event-planner') ?>  </label>
            </li>
            <li class="element-field">
                <input type="radio" name="sep_event_content" value="show-image"<?php echo $show_image; ?> >  <?php _e('Show Image', 'simple-event-planner') ?> 
            </li>
            <li class="element-field">
                <input type="radio" name="sep_event_content" value="hide-image"<?php echo $hide_image; ?>>  <?php _e('Hide Image', 'simple-event-planner') ?> 
            </li>
        </ul>
        
        <ul class="form-elements">
            <li class="field-label">
                <label> <?php _e('Time Format Settings', 'simple-event-planner') ?>  </label>
            </li>
            <li class="element-field">
                <input type="radio" name="sep_time_format" value="24-hours"<?php echo $hours_24; ?>>  <?php _e('24 Hours', 'simple-event-planner') ?> 
            </li>
            <li class="element-field">
                <input type="radio" name="sep_time_format" value="12-hours"<?php echo $hours_12; ?>>  <?php _e('12 Hours', 'simple-event-planner') ?> 
            </li>
        </ul>
        
        <div class="clear"></div>
        <?php
    }

}