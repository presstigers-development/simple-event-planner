$(document).ready(function () {
    $('.list-switch input:checkbox').change(function () {
        // gets data-group value and uses it in the outer selector
        // to select the inputs it controls and sets their disabled 
        // property to the negated value of it's checked property 
        $("." + $(this).data("group")).prop('disabled', !this.checked);
    }).change();
});