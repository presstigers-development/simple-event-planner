<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'presstig_plugin_sep');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'U/IMRv6aWWtl fO^ SX#l6BBjma6XvH2*/a16/|0t6)M![f5+{u-mc5W0!v0kT1 ');
define('SECURE_AUTH_KEY',  'oyq>(DS[E/<gcAJ|&Da[]sdPM&x5=7u{A%`J/vG@|H:nGqTCL@{s{x.!+J3yJ!&W');
define('LOGGED_IN_KEY',    'R`<JodI&[?A$9]e-F}M&*_ob&Z({(/n7rtK>DjoVxPPgkcaU15^nE.;{<CJZeg^N');
define('NONCE_KEY',        'T.VQY!CrFZ%i6W=A-UrOZ$EEDJdZI%q#mAgkK.y+7kp$Pxw|2X=d2l;]/`ScO`>0');
define('AUTH_SALT',        'uXyn)gGq<T9|7dJ$w}Db@W#}R]4Yw_i|P]Y13drukDZU-a m9S d;RR[BCmBR6#O');
define('SECURE_AUTH_SALT', '-G9,|S)uN]cAE|Y H6etQG$_c*N9ZBH-c]|[7n22(?FZ;&GEMLDq&[Ec-6Ip5(gg');
define('LOGGED_IN_SALT',   'OqB]cSl<U!%yhxyn9{$:$=%>=1Siq5|-le(<i(%N>z4Rso5~W.;MDe,Y-`LPUp0K');
define('NONCE_SALT',       '`MdW6_f7zG4_,dE=DM]tFZrc7Mb_eB3%lOny/I/;|-gXx{075rfIf7enae8;Yo7s');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'sep_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
