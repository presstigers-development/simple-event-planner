<?php

/**
 * Template Functions
 *
 * Template functions specifically created for event listings
 *
 * @author 	PressTigers
 * @category 	Core
 * @package    Simple_Event_Planner
 * @subpackage Simple_Event_Planner/public/partials
 * @version     1.0
 * @since       1.4
 */
$sep_event_options = get_option( 'sep_event_options' );
$fb_app_id = isset( $sep_event_options['sep_app_id'] ) ? $sep_event_options['sep_app_id'] : '';
$fb_app_secret = isset( $sep_event_options['sep_app_sec_id'] ) ? $sep_event_options['sep_app_sec_id'] : '';
$fb_page_id = isset( $sep_event_options['sep_fb_app_id'] ) ? $sep_event_options['sep_fb_app_id'] : '';
$fb_graph_url = 'https://graph.facebook.com/v2.10/';

/**
 * Generate Facebook api URL for grab Event.
 *
 * @since 1.4.0
 */
function fb_get_access_token( $fb_graph_url, $fb_app_id, $fb_app_secret ) {

	$sep_event_options = get_option( 'sep_event_options' );
	$fb_app_id = isset( $sep_event_options['sep_app_id'] ) ? $sep_event_options['sep_app_id'] : '';
	$fb_app_secret = isset( $sep_event_options['sep_app_sec_id'] ) ? $sep_event_options['sep_app_sec_id'] : '';
	$fb_page_id = isset( $sep_event_options['sep_fb_app_id'] ) ? $sep_event_options['sep_fb_app_id'] : '';
	$fb_graph_url = 'https://graph.facebook.com/v2.10/';




	$args = array(
		'grant_type' => 'client_credentials',
		'client_id' => $fb_app_id,
		'client_secret' => $fb_app_secret
	);
	$access_token_url = add_query_arg( $args, $fb_graph_url . 'oauth/access_token' );
	$access_token_response = wp_remote_get( $access_token_url );
	$access_token_response_body = wp_remote_retrieve_body( $access_token_response );
	$access_token_data = json_decode( $access_token_response_body );
	$access_token = $access_token_data->access_token;
	return $access_token;
}

/**
 * Generate Facebook api URL for grab Event.
 *
 * @since 1.4.0
 */
function get_facebook_response() {

	$sep_event_options = get_option( 'sep_event_options' );
	$fb_app_id = isset( $sep_event_options['sep_app_id'] ) ? $sep_event_options['sep_app_id'] : '';
	$fb_app_secret = isset( $sep_event_options['sep_app_sec_id'] ) ? $sep_event_options['sep_app_sec_id'] : '';
	$fb_page_id = isset( $sep_event_options['sep_fb_app_id'] ) ? $sep_event_options['sep_fb_app_id'] : '';
	$fb_graph_url = 'https://graph.facebook.com/v2.10/';



	$access_token = fb_get_access_token( $fb_graph_url, $fb_app_id, $fb_app_secret );
	$response = $access_token;

	return $response;
}

/**
 * Get event list on endpoint.
 *
 * @since 1.4.0
 */
function fb_get_eventslist() {

	$sep_event_options = get_option( 'sep_event_options' );
	$fb_app_id = isset( $sep_event_options['sep_app_id'] ) ? $sep_event_options['sep_app_id'] : '';
	$fb_app_secret = isset( $sep_event_options['sep_app_sec_id'] ) ? $sep_event_options['sep_app_sec_id'] : '';
	$fb_page_id = isset( $sep_event_options['sep_fb_app_id'] ) ? $sep_event_options['sep_fb_app_id'] : '';
	$fb_graph_url = 'https://graph.facebook.com/v2.10/';



	$fb_access_token = get_facebook_response();
	$events_arg = array( 'name', 'description', 'start_time', 'end_time', 'place', 'cover' );
	$event_params = implode( ",", $events_arg );

	$args = array(
		'fields' => 'events{' . $event_params . '}',
		'access_token' => $fb_access_token,
	);
	$fb_endpoint_url = add_query_arg( $args, $fb_graph_url . $fb_page_id );
	$fb_events_body_response = wp_remote_get( $fb_endpoint_url );
	$fb_events_response_body = wp_remote_retrieve_body( $fb_events_body_response );
	$fb_events_data = json_decode( $fb_events_response_body );
	return $fb_events_data;
}
