<?php

if ( !defined( 'ABSPATH' ) ) {
	exit;
} // Exit if accessed directly
/**
 * Simple_Event_Planner_Shortcode_Facebook_Events class
 *
 * Facebook Events Shortcode.
 *
 * This class lists the events on frontend for [sep_fb_shortcode] shortcode. It 
 * lists all upcoming events in the list with event search bar.
 * 
 * @link        https://wordpress.org/plugins/simple-event-planner/
 * @since       1.4.0
 * 
 * @package     Simple_Event_Planner
 * @subpackage  Simple_Event_Planner/includes/shortcodes
 * @author      PressTigers <support@presstigers.com>
 */

class Simple_Event_Planner_Shortcode_Facebook_Events {

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since   1.4.0
	 * 
	 * @return  void
	 */
	public function __construct() {

		// Hook-> Event Listing Shortcode
		add_shortcode( 'sep_fb_shortcode', array( $this, 'sep_social_shortcode_function' ) );
	}

	/**
	 * Event Listing Shortcode
	 *
	 * @since   1.4.0
	 * 
	 * @param   string  $attr   Shortcode Parameters
	 * @return  void 
	 */
	public function sep_social_shortcode_function( $atts, $content ) {

		global $item;
		/**
		 * Template -> Start Wrapper:
		 * 
		 * - Event Start Wrapper
		 */
		/* handle the result */
		$fb_event_data = fb_get_eventslist();
		/**
		 * Template -> Event Lisiting Start:
		 * 
		 * - Event Listing Start
		 */
		get_simple_event_planner_template( 'event-listing/start-wrapper.php' );

		/**
		 * Template -> Event Lisiting Start:
		 * 
		 * - Event Listing Start
		 */
		get_simple_event_planner_template( 'event-listing/event-listings-start.php' );
		if ( !empty( $fb_event_data->events->data ) ) {
			foreach ( $fb_event_data->events->data as $item ) {
				get_simple_event_planner_template( 'content-facebookevent-listing-list-view.php' );
//				if ( !empty( $item->place->location->latitude || $item->place->location->longitude ) ) {
//					echo 'Event Location(Lat): ' . $item->place->location->latitude . "<br>";
//					echo 'Event Location(Long): ' . $item->place->location->longitude . "<br>";
//				}
			}
		}

		/**
		 * Template -> Event Lisiting End:
		 * 
		 * - Event Listing End Wrapper
		 */
		get_simple_event_planner_template( 'event-listing/event-listings-end.php' );

		/**
		 * Template -> End Wraper:
		 * 
		 * - Event End Wrapper
		 */
		get_simple_event_planner_template( 'event-listing/end-wrapper.php' );
	}

}
